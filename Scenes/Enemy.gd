extends Area2D

var screensize
# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var speed = 100
export var forward_step = 30
var direction

func _ready():
	screensize = get_viewport_rect().size
	direction = "left"
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#pass

func _process(delta):
	var velocity = 0
	if direction == "left":
		velocity = -1
	else:
		velocity = 1
		
	position.x += velocity * speed * delta
	
	if position.x < -0 or position.x > screensize.x + 0:
		position.y += forward_step
		if direction == "left":
			direction = "right"
		else:
			direction = "left"
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
