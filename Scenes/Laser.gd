extends Area2D

export var speed = 500
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
	position.y -= speed * delta
	if position.y < -50:
		queue_free()
	
func _on_Laser_area_entered(area):
	if area.is_in_group("enemy"):
		area.queue_free()
		queue_free()
